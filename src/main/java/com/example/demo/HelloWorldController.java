package com.example.demo;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HelloWorldController {
	@GetMapping("/")
	@ResponseBody
	public Map<String, String> sayHello(@RequestParam(name="name", required=false, defaultValue="Micho") String name) {
		Map<String, String> map = new HashMap<>();
		map.put("message", "Hello from Spring!");
		map.put("name", name);
		return map;
	}
}
